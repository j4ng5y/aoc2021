pub fn sonar_sweep(vec: Vec<i32>) -> i32 {
    let mut first_pos: i32 = 0;
    let mut second_pos: i32 = 1;
    let mut count: i32 = 0;

    while second_pos < vec.len() as i32 {
        if is_bigger(vec[first_pos as usize], vec[second_pos as usize]) {
            count = count + 1
        }
        first_pos = dbg!(first_pos + 1);
        second_pos = dbg!(second_pos + 1);
    }

    count
}

pub fn sliding_sonar_sweep(vec: Vec<i32>) -> i32 {
    let mut first_triple_pos: usize = 0;
    let mut second_triple_pos: usize = 1;
    let mut count: i32 = 0;

    while second_triple_pos+2 < vec.len() {
        let first_triple_sum: i32 = dbg!(vec[first_triple_pos]+vec[first_triple_pos+1]+vec[first_triple_pos+2]);
        let second_triple_sum: i32 = dbg!(vec[second_triple_pos]+vec[second_triple_pos+1]+vec[second_triple_pos+2]);
        if is_bigger(first_triple_sum, second_triple_sum) {
            count = dbg!(count + 1)
        }
        first_triple_pos = dbg!(first_triple_pos + 1);
        second_triple_pos = dbg!(second_triple_pos + 1);
    }
    count
}

fn is_bigger(first: i32, second: i32) -> bool {
    return if second > first {
        true
    } else {
        false
    }
}

#[cfg(test)]
mod day1_tests {
    use crate::day1::{sliding_sonar_sweep, sonar_sweep};

    fn gen_vec() -> Vec<i32> {
        let vec: Vec<i32> = vec![
            199,
            200,
            208,
            210,
            200,
            207,
            240,
            269,
            260,
            263,
        ];

        vec
    }

    #[test]
    fn test_sonar_sweep() {
        let n: i32 = sonar_sweep(gen_vec());
        assert_eq!(n, 7)
    }

    #[test]
    fn test_sliding_sonar_sweep() {
        let n: i32 = sliding_sonar_sweep(gen_vec());
        assert_eq!(n, 5)
    }
}