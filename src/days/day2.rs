#[derive(Debug)]
struct Grid {
    horizontal_pos: i32,
    depth: i32,
    aim: i32,
}

impl Grid {
    fn new() -> Grid {
        Grid{horizontal_pos: 0, depth: 0, aim: 0}
    }

    fn final_calculation(&self) -> i32 {
        dbg!(self.horizontal_pos*self.depth)
    }

    fn nav(&mut self, command: &str, n: i32) {
        match command {
            "forward" => self.horizontal_pos = dbg!(self.horizontal_pos + n),
            "up" => self.depth = dbg!(self.depth - n),
            "down" => self.depth = dbg!(self.depth + n),
            _ => println!("invalid command: {}", command)
        }
    }

    fn nav_with_aim(&mut self, command: &str, n: i32) {
        match command {
            "forward" => {
                self.horizontal_pos = dbg!(self.horizontal_pos + n);
                self.depth = dbg!(self.depth + (n * self.aim))
            },
            "up" => self.aim = dbg!(self.aim - n),
            "down" => self.aim = dbg!(self.aim + n),
            _ => println!("invalid command: {}", command)
        }
    }
}

pub fn do_nav_calc(vec: Vec<(String, i32)>, aim: bool) -> i32 {
    let mut g = Grid::new();

    match aim {
        true => {
            for value in vec {
                g.nav_with_aim(value.0.as_str(), value.1)
            }
        },
        false => {
            for value in vec {
                g.nav(value.0.as_str(), value.1)
            }
        },
    }

    g.final_calculation()
}

#[cfg(test)]
mod day2_tests {
    use crate::day2::Grid;

    fn gen_test_vec() -> Vec<(String, i32)> {
        let vec: Vec<(String, i32)> = vec![
            ("forward".to_string(), 5),
            ("down".to_string(), 5),
            ("forward".to_string(), 8),
            ("up".to_string(), 3),
            ("down".to_string(), 8),
            ("forward".to_string(), 2)];
        vec
    }

    #[test]
    fn test_grid_nav() {
        let mut g = Grid::new();

        for value in gen_test_vec() {
            g.nav(value.0.as_str(), value.1)
        }

        assert_eq!(g.final_calculation(), 150)
    }

    #[test]
    fn test_grid_nav_with_aim() {
        let mut g = Grid::new();

        for value in gen_test_vec() {
            g.nav_with_aim(value.0.as_str(), value.1)
        }

        assert_eq!(g.final_calculation(), 900)
    }
}