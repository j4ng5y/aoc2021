use std::str::FromStr;

pub fn input_to_vec_i32(content: String) -> Vec<i32> {
    let mut vec: Vec<i32> = vec![];

    for line in content.lines() {
        let n: i32 = FromStr::from_str(line).unwrap();
        vec.push(n)
    }

    vec
}

pub fn input_to_vec_str_i32(content: String) -> Vec<(String, i32)> {
    let mut vec: Vec<(String, i32)> = Vec::new();

    for line in content.lines() {
        let split = line.split(" ");
        let vec2: Vec<&str> = split.collect();
        let s: String = FromStr::from_str(vec2[0]).unwrap();
        let n: i32 = FromStr::from_str(vec2[1]).unwrap();
        let v: (String, i32) = (s, n);
        vec.push(v)
    }

    vec
}