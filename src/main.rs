mod days;

use std::path::PathBuf;
use structopt::StructOpt;
use days::*;

#[derive(StructOpt)]
struct Cli {
    #[structopt(short = "d", long = "day")]
    day: Option<String>,
    #[structopt(short = "f", long = "file", parse(from_os_str))]
    input: PathBuf
}

fn main() {
    let args = Cli::from_args();
    let mut day: String = "".to_string();
    let content: String = std::fs::read_to_string(&args.input).expect("could not read input file");

    match args.day {
        Some(i) => day = i,
        None => println!("no day selected")
    }

    call_day(day.as_str(), content)
}

fn call_day(arg: &str, content: String) {
    match arg {
        "0" => println!("there is no zero day, this is rust :D"),
        "1_1" => {
            println!("{}", day1::sonar_sweep(utils::input_to_vec_i32(content)))
        },
        "1_2" => {
            println!("{}", day1::sliding_sonar_sweep(utils::input_to_vec_i32(content)))
        },
        "2_1" => {
            println!("{}", day2::do_nav_calc(utils::input_to_vec_str_i32(content), false))
        },
        "2_2" => {
            println!("{}", day2::do_nav_calc(utils::input_to_vec_str_i32(content), true))
        },
        "3_1" => println!("day {}", arg),
        "3_2" => println!("day {}", arg),
        "4_1" => println!("day {}", arg),
        "4_2" => println!("day {}", arg),
        "5_1" => println!("day {}", arg),
        "5_2" => println!("day {}", arg),
        "6_1" => println!("day {}", arg),
        "6_2" => println!("day {}", arg),
        "7_1" => println!("day {}", arg),
        "7_2" => println!("day {}", arg),
        "8_1" => println!("day {}", arg),
        "8_2" => println!("day {}", arg),
        "9_1" => println!("day {}", arg),
        "9_2" => println!("day {}", arg),
        "10_1" => println!("day {}", arg),
        "10_2" => println!("day {}", arg),
        "11_1" => println!("day {}", arg),
        "11_2" => println!("day {}", arg),
        "12_1" => println!("day {}", arg),
        "12_2" => println!("day {}", arg),
        "13_1" => println!("day {}", arg),
        "13_2" => println!("day {}", arg),
        "14_1" => println!("day {}", arg),
        "14_2" => println!("day {}", arg),
        "15_1" => println!("day {}", arg),
        "15_2" => println!("day {}", arg),
        "16_1" => println!("day {}", arg),
        "16_2" => println!("day {}", arg),
        "17_1" => println!("day {}", arg),
        "17_2" => println!("day {}", arg),
        "18_1" => println!("day {}", arg),
        "18_2" => println!("day {}", arg),
        "19_1" => println!("day {}", arg),
        "19_2" => println!("day {}", arg),
        "20_1" => println!("day {}", arg),
        "20_2" => println!("day {}", arg),
        "21_1" => println!("day {}", arg),
        "21_2" => println!("day {}", arg),
        "22_1" => println!("day {}", arg),
        "22_2" => println!("day {}", arg),
        "23_1" => println!("day {}", arg),
        "23_2" => println!("day {}", arg),
        "24_1" => println!("day {}", arg),
        "24_2" => println!("day {}", arg),
        "25_1" => println!("day {}", arg),
        "25_2" => println!("day {}", arg),
        _ => println!("nah, day {} isn't a thing here", arg)
    }
}